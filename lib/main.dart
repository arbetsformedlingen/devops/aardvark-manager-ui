import 'package:aardvark_manager/CreateProjectForm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Environment.dart';
import 'model/Options.dart';
import 'model/Project.dart';

Future<void> main() async {
  await dotenv.load(fileName: Environment.fileName);
  var options = OptionsModel();
  options.fetchEnvironments();
  options.fetchTeams();
  runApp(
    ChangeNotifierProvider(
      create: (context) => options,
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final TextEditingController _controller = TextEditingController();
  final _environments = <String, bool>{};
  String? _ownerTeam;
  Future<Project>? _futureProject;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aardvark Manager',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Aardvark Manager'),
        ),
        body: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(8.0),
          child: (_futureProject == null) ? buildForm() : buildResponseView(),
        ),
      ),
    );
  }

  Column buildForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "GitLab project path ",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        TextField(
          controller: _controller,
          decoration: const InputDecoration(
              hintText: 'Path to source project within GitLab'),
        ),
        Consumer<OptionsModel>(
            builder: (context, options, child) => Column(children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Owner Team: ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      DropdownButton<String>(
                        value: _ownerTeam,
                        icon: const Icon(Icons.arrow_downward),
                        underline: Container(
                          height: 2,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            _ownerTeam = newValue!;
                          });
                        },
                        items: options.teams
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      )
                    ],
                  ),
                  Text("Desired environments: ",
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  Column(
                      children: options.environments
                          .map((env) => CheckboxListTile(
                              title: Text(env),
                              value: _environments.containsKey(env)
                                  ? _environments[env]
                                  : false,
                              onChanged: (bool? value) {
                                setState(() {
                                  _environments[env] = value!;
                                });
                              }))
                          .toList())
                ])),
        ElevatedButton(
          onPressed: () {
            // TODO: Validate team is selected.
            setState(() {
              _futureProject = createProject(_controller.text, _environments, _ownerTeam ?? "");
            });
          },
          child: const Text('Create project'),
        ),
      ],
    );
  }

  FutureBuilder<Project> buildResponseView() {
    return FutureBuilder<Project>(
      future: _futureProject,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Source code project:',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                      child: Text(
                        snapshot.data!.srcProject.name,
                        style: const TextStyle(color: Colors.blue),
                      ),
                      onTap: () => launchUrl(
                          Uri.parse(snapshot.data!.srcProject.httpRepoUrl))),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Infrastructure project:',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                      child: Text(
                        snapshot.data!.infraProject.name,
                        style: const TextStyle(color: Colors.blue),
                      ),
                      onTap: () => launchUrl(
                          Uri.parse(snapshot.data!.infraProject.httpRepoUrl))),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Merge request for namespaces:',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                      child: Text(
                        snapshot.data!.mergeRequestNamespaces,
                        style: const TextStyle(color: Colors.blue),
                      ),
                      onTap: () => launchUrl(
                          Uri.parse(snapshot.data!.mergeRequestNamespaces))),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Merge request for ArgoCD:',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                      child: Text(
                        snapshot.data!.mergeRequestArgocd,
                        style: const TextStyle(color: Colors.blue),
                      ),
                      onTap: () => launchUrl(
                          Uri.parse(snapshot.data!.mergeRequestArgocd))),
                ],
              ),
              const Text('Merge the two merge requests above.',
                    style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const Text('Apply permissions for Aardvark to the develop namespace.',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const Text('Do a change in the source code project to trigger a build.',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const Text('Adapt the manifests in the infrastructure project if needed.',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const Text(''),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    _controller.text = '';
                    _futureProject = null;
                  });
                },
                child: const Text('Create another project'),
              )
            ],
          );
        } else if (snapshot.hasError) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('${snapshot.error}'),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    _futureProject = null;
                  });
                },
                child: const Text('Another try to create project'),
              )
            ],
          );
        }
        return const CircularProgressIndicator();
      },
    );
  }
}
