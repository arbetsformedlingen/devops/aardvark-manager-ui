class Environments {
  final List<String>  environments;

  const Environments({
    required this.environments,
  });

  factory Environments.fromJson(Map<String, dynamic> json) {
    final envData = json['environments'] as List<dynamic>?;
    final envList = envData != null ? envData.map((env) => env['name'] as String).toList() : <String>[];
    return Environments(environments: envList);
  }
}
